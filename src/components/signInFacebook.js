// signInFacebook.js

import { ref } from 'vue'
import { getAuth, FacebookAuthProvider, signInWithPopup} from 'firebase/auth'
 
export default function signInFacebook(aa) {
  
    return {
      facebook 
    };

 function facebook() {

    const auth = getAuth();
    auth.languageCode = 'tr';
    const provider = new FacebookAuthProvider();
    
    return signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Facebook Access Token. You can use it to access the Google API.
        const credential =  FacebookAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        //console.log('token:',credential, ' user:', user)
        return user;
        // ...
      }).catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        // !! const email = error.customData.email;
        // The AuthCredential type that was used.
        const credential = FacebookAuthProvider.credentialFromError(error);
        // ...
      });
      
      
  }
  
     // Install
     // https://firebase.google.com/docs/auth/web/facebook-login
     // https://softauthor.com/facebook-login-for-ios-using-firebaseui/
     // 1) firebase->authentication->sign in method->facebook-> 
     //     -- id ve secret i gir.
     //     -- sayfanın altındaki linki copyala.(bu "OAuth redirect" sayfasıdır. )
     //     -- https://ilcine-12b5d.firebaseapp.com/__/auth/handler
     //     -- bu link facebook taki oAuth sayfasına konulacak (-facebook login-->setting->"Valid OAuth Redirect URIs")
     
     // 2) facebook sayfası hak.
     //    https://developers.facebook.com/
     // 3) facebook hesabı aç // varsa gerek yok.
     // 4) Facebook geliştirici hesabına kayıtlı gerekir
     //    -- "My app" --> create App --> hiçbiri(or etc) --> app name:pwStore --> email:emr@uludag.edu.tr -->
     //    --> setting --> basic--( app.id ve app.secret i al)
     //    app.id: 700495114789975
     //    app.secret: e6dce6d85a3ebe2fbf8e23dfb8144703
     //    facebook-developer-->setting--> basic -->( email vb gir) --> advanced --> ( facebook login seç, sol menide belirecek)
     //    -facebook login-->setting->"Valid OAuth Redirect URIs"
  
  
}   
 