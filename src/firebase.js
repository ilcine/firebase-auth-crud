//firebase.js 

import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { firebaseConfig } from './firebaseConfig'

try {
      console.log("initialize the firebase app");
      initializeApp(firebaseConfig)
    } catch (err) 
    {
      console.error("Firebase initialization error", err.stack);
    }
    
//initialize firebase auth
const auth = getAuth()

//export the auth object
export { auth }

