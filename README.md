# 1) firebase-auth-crud install 

## 1.1) git clone https://gitlab.com/ilcine/firebase-auth-crud.git
## 1.2) cd firebase-auth-crud

# 2) Create Firebase DB

## 2.1) https://console.firebase.google.com

## 2.2) Create project button.

## 2.3) Create a database; "select Build" > "Firestore Database" .
### 2.3.1) select: firestore location 
### 2.3.2) register app;  

## 2.4) "Firestore Databes" --> rules
 
<pre>
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
  
    // True if the user is signed in or the requested data is 'public'
    function signedInOrPublic() {
      return request.auth.uid != null || resource.data.visibility == 'public';
    }
    
    match /my-users/{user} {       
       allow read, write: if signedInOrPublic();
    }
      
  }
}
</pre>
 
## 2.5) Authentication 

### 2.5.1) "Autentication" --> sign-in method -->Sign-in providers--> e-mail/passwd enabled
### 2.5.2) "Autentication" --> users --> "add user" (enter email and password, fake or real)
### 2.5.3) "Proje Overview(enter wheel icon)" --> project setting. --> see: firebaseConfig 

## 2.6) copy firebaseConfig.txt to firebaseConfig.js and edit firebaseConfig.js

<pre>
 
const firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: "",
  measurementId: ""
};

export { firebaseConfig }
 
</pre>

# 3) run

## 3.1) npm i
## 3.2) npm run dev
## 3.3) http://localhost:8080/#/sign-in 
## 3.4) Enter generated mail and pw in firebase login

# 4) other links

<pre>
https://firebase.google.com/docs/firestore/data-model
https://firebase.google.com/docs/firestore/data-model#hierarchical-data
Cloud Firestore, NoSQL, belge odaklı bir veritabanıdır. 
 SQL veritabanından farklı olarak, tablo veya satır yoktur. 
 Bunun yerine, verileri koleksiyonlar halinde düzenlenen belgelerde depolarsınız .

Her belge bir dizi anahtar/değer çifti içerir. 
Cloud Firestore, büyük küçük belge koleksiyonlarını depolamak için optimize edilmiştir.

Tüm belgeler koleksiyonlarda saklanmalıdır. 
  Belgeler, her ikisi de dizeler gibi ilkel alanlar veya 
  listeler gibi karmaşık nesneler içerebilen alt koleksiyonlar ve iç içe nesneler içerebilir.

Koleksiyonlar ve belgeler, Cloud Firestore'da dolaylı olarak oluşturulur. 
Bir koleksiyon içindeki bir belgeye veri atamanız yeterlidir. 
Koleksiyon veya belge yoksa, Cloud Firestore onu oluşturur.

https://modularfirebase.web.app/common-use-cases/firestore/


https://towardsdatascience.com/sql-vs-nosql-join-operations-401f18a8a53b
https://firebase.google.com/docs/firestore/solutions/aggregation
https://medium.com/@joaqcid/how-to-inner-join-data-from-multiple-collections-on-angular-firebase-bfd04f6b36b7
switchMap and combineLatest.

</pre>
