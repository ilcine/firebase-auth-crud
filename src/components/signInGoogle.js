// signInGoogle.js

import { ref, onMounted } from 'vue'
import { getAuth, GoogleAuthProvider, signInWithPopup } from 'firebase/auth'

export default function signInGoogle() {   

  function google(r) {
    //console.log('signGoogle:',r)
    const auth = getAuth();
    auth.languageCode = 'tr';    
    const provider = new GoogleAuthProvider();
  
    return signInWithPopup(auth, provider)
      .then((result) => {
        const credential =  GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        const user = result.user;
        
        return user
      }).catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        // !!  const email = error.customData.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
        // !!    console.log('errorEmail:', error.customData.email)
      });
  }
   
  return {
    google, 
  };
  
  template: `
      <div>
            Notes
      </div>  
    `
  
  // Install
  // https://firebase.google.com/docs/auth/web/google-signin
  // 1) firebase->authentication->sign in method->Google-> add. save. 

  // otherlink: https://dev.to/itscasey/vue-firebase-google-easy-authentication-20ac
}   
 