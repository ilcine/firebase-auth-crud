import { createStore } from 'vuex'

export default createStore({ 
   
  state: {
    user: {
      isLoggedIn: false,
      displayName: '',
      email: '',
      data: null
    }
  },

  getters: {
    user(state){
     return state.user
    }
  },

  mutations: {
    
    SET_USER(state, value) {
    state.user = value;
    },
    
  },

  actions: {
  
    CHANGE_USER(context, value) {
       context.commit('SET_USER', value)
       //console.log('vuex SET user ok:',value);	     
    },
  }

})
    
// ---- USE --- //  
// for SET
// const status = 'ok' // or 'not ok'
// store.dispatch('CHANGE_LOGGED_IN',status)   // USE for set ( status: ok or not ok value )	

//  for DISPLAY
//  in template
//  {{ vuexUser }} 
//  in JS
//  import { computed } from 'vue'
//  const store = useStore()
//  const vuexUser = computed(() => {
//   return store.getters.user;
// });
      

