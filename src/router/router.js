
import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import { getAuth,onAuthStateChanged } from 'firebase/auth'

const routes = [    
      {
        path: '/',
        name: 'root',  /* root */
        component: () => import('../components/home.vue'),
        //component: () => import('../components/navNoAuth.vue')
        //meta: { authRequired: false, },
      },
      {
        path: '/home',
        name: 'Home',
        component: () => import('../components/home.vue')
      },
      {
        path: '/nav-no-auth',
        name: 'navNoAuth',
        component: () => import('../components/navNoAuth.vue')
      },
      {
        path: '/nav-auth',
        name: 'navAuth',
        component: () => import('../components/navAuth.vue')
      },
      
      {
        path: '/register',
        name: 'register',
        component: () => import('../components/register.vue'),
        //meta: { authRequired: false, },
      },
      
      {
        path: '/sign-in',
        component: () => import('../components/signIn.vue'),
        //meta: { authRequired: false, },
      },
      
      {
        path: '/sign-in-firebase-pw',
        component: () => import('../components/signInFirebasePw.vue'),
        //meta: { authRequired: false, },
      },
      
      {
        path: '/sign-in-phone',
        name:'signInPhone',
        component: () => import('../components/signInPhone.vue'),
        //meta: { authRequired: false, },
      },
      
      {
        path: '/feed',
        name: 'feed',
        component: () => import('../components/feed.vue'),
        meta: { authRequired: true, }, 
        // App.vue feed yetki dışı olduğu için burada tanımlandı
        // ayrıca router.beforeEach de tanım yapıldığı için bu meta gerekli
        // diğerlerini zazen App.vue koruyor.
       
      },

      {
        path: '/docExample',
        name: 'docExample',
        component: () => import('../components/docExample.vue'),
       // meta: { requiresAuth: true }, 
      },
      
      {
        path: '/docNew',
        name: 'docNew',
        component: () => import('../components/docNew.vue'),
       // meta: { requiresAuth: true }, 
      },
      
      {
        path: '/docEdit/:id',
        name: 'docEdit',
        component: () => import('../components/docEdit.vue'),
        props: true,  // önemli 
        // meta: { requiresAuth: true }, 
      },
   
      /*
      {
        path: '/docDelete',
        name: 'docDelete',
        component: () => import('../components/docDelete.vue')
      }
      */
 ]
  
const router = createRouter({
  history: createWebHashHistory(),
  mode: 'hash',
  routes
})

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some((record) => record.meta.authRequired)
  if (requiresAuth && !(await getCurrentUser())) {
      next({path: '/',})
  } else {  next() }
})

//ok.
function getCurrentUser() {
  return new Promise((resolve, reject) => {
    const unsubscribe = onAuthStateChanged( getAuth(), (user) => {
         unsubscribe()
         if (user) { console.log('ok:User:Auth:',user.displayName) }
         resolve(user)
      }, reject
    )
  })
}

/*
//ok  
const getCurrentUser2 = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = onAuthStateChanged( getAuth(), (userFirebase) => {
      unsubscribe()
      resolve(userFirebase)
    }, reject)

  })
}
*/

export default router
