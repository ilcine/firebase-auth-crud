import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router/router' // vue-router
import store from "./store/store"  // vuex
import { auth } from './firebase'
import navNoAuth from '@/components/navNoAuth.vue';
import navAuth from '@/components/navAuth.vue';

const app = createApp(App)

app.use(router)
app.use(store)
app.component('navNoAuth', navNoAuth);
app.component('navAuth', navAuth);

app.mount('#app')
